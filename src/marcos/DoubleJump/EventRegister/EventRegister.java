package marcos.DoubleJump.EventRegister;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

public class EventRegister implements Listener {

	@EventHandler
	public void PlayerInteract(PlayerToggleFlightEvent event) {

		Player player = event.getPlayer(); // Criei uma vari�vel da classe 'Player' que importei do bukkit e adicionei
											// um evento ao player.
		if (player.getGameMode() != GameMode.CREATIVE) {
			player.setAllowFlight(false);
			event.setCancelled(true); // ele vai cancelar a a��o de voar
			Vector v = player.getLocation().getDirection().multiply(1).setY(1);
			player.setVelocity(v);
		}	
	}
	
	@EventHandler
	public void playerCanFly(PlayerMoveEvent event) {

		Player player = event.getPlayer();
		if(player.getGameMode()!=GameMode.CREATIVE){
		// Se o jogo for diferente de criativo, ele vai cancelar o
		// evento
		Block b = player.getWorld().getBlockAt(player.getLocation().subtract(0, 1, 0)); // Criei uma vari�vel bloco
		// que vai ser igual �
		// vari�vel player
		if (!b.getType().equals(Material.AIR)) {
			event.getPlayer().setAllowFlight(true);
			}
		}
	}
}
